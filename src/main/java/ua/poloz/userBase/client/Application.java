package ua.poloz.userBase.client;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.user.client.Timer;
import ua.poloz.userBase.shared.dto.UserDTO;
import ua.poloz.userBase.shared.services.UserServiceAsync;

import java.util.*;

public class Application
        implements EntryPoint {

    private static final String SERVER_ERROR = "An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : ";
    private final UserServiceAsync userService = UserServiceAsync.Util.getInstance();    
    private final List<UserDTO> CONTACTS = new ArrayList();
    

    public void onModuleLoad() {
        final Set<Long> userDel = new HashSet<Long>();
        final CellTable<UserDTO> table = new CellTable();
        final Button saveOrUpdateButton = new Button("Save");
        final Button addUserButton = new Button("Add user");
        final Button retrieveButton = new Button("Delete");
        final Button exportBase = new Button("Export to PDF");
        final Button closeButton = new Button("OK");
        final Button update = new Button("Update");
        final TextBox userNameField = new TextBox();
        final TextBox userSurnameField = new TextBox();
        final TextBox userPhoneNumberField = new TextBox();
        userNameField.setText("name");
        userSurnameField.setText("surname");
        userPhoneNumberField.setText("phone number");
        final TextBox editUserNameField = new TextBox();
        final TextBox editUserSurnameField = new TextBox();
        final TextBox editUserPhoneNumber = new TextBox();
        final Label editUserId = new Label();
        final Button cancelEditUser = new Button("Cancel");
        final TextBox userIdField = new TextBox();
        final Label errorLabel = new Label();
        final DialogBox addUser = new DialogBox();
        final DialogBox editUserBox = new DialogBox();
        final DialogBox dialogBox = new DialogBox();
        final Image image = new Image();
        final Label textToServerLabel = new Label();
        final HTML serverResponseLabel = new HTML();

        // кнопка удаления неактивна
        retrieveButton.setEnabled(false);

        // рисуем картинку
        image.setSize("250", "350");
        image.setUrl(GWT.getModuleBaseURL() + "phone book.jpg");

        // выводим все элементы на страницу
        RootPanel.get("retrieveUserButtonContainer").add(retrieveButton);
        RootPanel.get("errorLabelContainer").add(errorLabel);
        RootPanel.get("retrieveUserButtonContainer").add(image);
        RootPanel.get("addUserButtonContainer").add(addUserButton);
        RootPanel.get("userIdFieldContainer").add(table);
        RootPanel.get("userIdFieldContainer").add(exportBase);

        // устанавливаем значения по умолчанию для полей ввода информвции
        userNameField.setFocus(true);
        userNameField.selectAll();
        userSurnameField.selectAll();
        userPhoneNumberField.selectAll();

        dialogBox.setText("Remote Procedure Call");
        dialogBox.setAnimationEnabled(true);

        closeButton.getElement().setId("closeButton");

        // панель ответа сервера
        VerticalPanel dialogVPanel = new VerticalPanel();
        dialogVPanel.addStyleName("dialogVPanel");
        dialogVPanel.add(new HTML("<b>Sending request to the server:</b>"));
        dialogVPanel.add(textToServerLabel);
        dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
        dialogVPanel.add(serverResponseLabel);
        dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        dialogVPanel.add(closeButton);
        dialogBox.setWidget(dialogVPanel);

        // внутренний класс для получения списка юзеров
        class FindAll {

            private List<UserDTO> alles() {
                userService.allUser(new AsyncCallback<List<UserDTO>>() {
                    public void onFailure(Throwable caught) {
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel
                                .addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : " + caught.toString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    @Override
                    public void onSuccess(List<UserDTO> result) {
                        CONTACTS.clear();
                        CONTACTS.addAll(result);
                    }
                });
                return CONTACTS;
            }

            private Integer count() {
                userService.allUser(new AsyncCallback<List<UserDTO>>() {
                    public void onFailure(Throwable caught) {
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel
                                .addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : " + caught.toString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    public void onSuccess(List<UserDTO> result) {
                        CONTACTS.clear();
                        CONTACTS.addAll(result);
                    }
                });
                return Integer.valueOf(CONTACTS.size());
            }
        }
        ;
       final FindAll findAll = new FindAll();


//        рисуем таблицу
        table.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);

        final SelectionModel<UserDTO> selectionModel = new MultiSelectionModel<UserDTO>();
        table.setSelectionModel(selectionModel, DefaultSelectionEventManager.<UserDTO>createCheckboxManager());
        Column<UserDTO, Boolean> checkColumn = new Column<UserDTO, Boolean>(new CheckboxCell(true, false)) {
            public Boolean getValue(UserDTO object) {
                if (selectionModel.isSelected(object) == true)  {
                           userDel.add(object.getUserId());
                    retrieveButton.setEnabled(true);
            }  else {
                    userDel.remove(object.getUserId());
                    if (userDel.size()==0){
                    retrieveButton.setEnabled(false);
                    }
                }
                return selectionModel.isSelected(object);
            }
        };

        checkColumn.setFieldUpdater( new FieldUpdater<UserDTO, Boolean>() {
            @Override
            public void update(int index, UserDTO object, Boolean value) {
            }
        });

        table.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));

        TextColumn<UserDTO> idColumn = new TextColumn<UserDTO>() {
            public String getValue(UserDTO object) {
                return String.valueOf(object.getUserId());
            }
        };
        idColumn.setSortable(true);
        table.addColumn(idColumn, "id");

        TextColumn<UserDTO> nameColumn = new TextColumn<UserDTO>() {
            public String getValue(UserDTO object) {
                return object.getUserName();
            }
        };
        nameColumn.setSortable(true);
        table.addColumn(nameColumn, "Name");

        TextColumn<UserDTO> surnameColumn = new TextColumn<UserDTO>() {
            public String getValue(UserDTO object) {
                return object.getUserSurname();
            }
        };
        surnameColumn.setSortable(true);
        table.addColumn(surnameColumn, "Surname");

        TextColumn<UserDTO> phoneNumberColumn = new TextColumn<UserDTO>() {
            public String getValue(UserDTO object) {
                return object.getPhoneNumber();
            }
        };
        phoneNumberColumn.setSortable(true);
        table.addColumn(phoneNumberColumn, "Phone number");

        // кнопка редактирования
        ButtonCell editButton = new ButtonCell();
        Column<UserDTO, String> edit = new Column<UserDTO, String>(editButton) {
            @Override
            public String getValue(UserDTO userDTO) {
                return "update";
            }
        };
        edit.setFieldUpdater(new FieldUpdater<UserDTO, String>() {
            @Override
            public void update(int i, UserDTO userDTO, String s) {
                editUserId.setText(String.valueOf(userDTO.getUserId()));
                editUserNameField.setText(userDTO.getUserName());
                editUserSurnameField.setText(userDTO.getUserSurname());
                editUserPhoneNumber.setText(userDTO.getPhoneNumber());
                editUserBox.setText("Update user");
                editUserBox.center();
                editUserBox.show();
                table.redraw();
            }
        });
        table.addColumn(edit, "Update user");

        table.setRowData(0, findAll.alles());
        table.setRowCount(findAll.count(), true);

        table.redraw();
        // таймер для перерисовки таблицы после обновления страницы
        Timer t = new Timer() {
            @Override
            public void run() {
                long userId = 0;
                for (int i = 0; i < findAll.count(); i++) {
                    if (userId <= (findAll.alles().get(i)).getUserId()) {
                        userId = (findAll.alles().get(i)).getUserId() + 1;
                    }
                }
                table.setRowData(0, findAll.alles());
                table.setRowCount(findAll.count(), true);
                table.redraw();
            }
        };
        // устанавливае время в милисекундах
        t.schedule(500);

        // кнопка экспорта базы
        exportBase.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                Window.open("../Download",
                        "download", "");
            }
        });

        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                dialogBox.hide();
                saveOrUpdateButton.setEnabled(true);
                saveOrUpdateButton.setFocus(true);

                table.setRowData(findAll.alles());
                table.setRowCount(findAll.count(), true);

                table.setRowData(0, findAll.alles());
                table.redraw();
            }
        });

        // кнопка закрытия панели редактирования
        cancelEditUser.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                editUserBox.hide();
            }
        });

        // панель редактирования юзера
        VerticalPanel editVerticalPanel = new VerticalPanel();
        editVerticalPanel.add(editUserId);
        editVerticalPanel.add(editUserNameField);
        editVerticalPanel.add(editUserSurnameField);
        editVerticalPanel.add(editUserPhoneNumber);
        editVerticalPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        HorizontalPanel editHorisontalPanel = new HorizontalPanel();
        editHorisontalPanel.add(update);
        editHorisontalPanel.setVerticalAlignment(HorizontalPanel.ALIGN_BOTTOM);
        editHorisontalPanel.add(cancelEditUser);
        editVerticalPanel.add(editHorisontalPanel);
        editUserBox.add(editVerticalPanel);

        // кнопка отмены добавления нового юзера
        Button back = new Button("Back");
        back.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addUser.hide();
            }
        });

        // панель добавления нового юзера
        VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.setSpacing(5);
        verticalPanel.add(userNameField);
        verticalPanel.add(userSurnameField);
        verticalPanel.add(userPhoneNumberField);
        verticalPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.add(saveOrUpdateButton);
        horizontalPanel.setVerticalAlignment(HorizontalPanel.ALIGN_BOTTOM);
        horizontalPanel.add(back);
        verticalPanel.add(horizontalPanel);
        addUser.setWidget(verticalPanel);

        addUserButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addUser.setText("Add user");
                addUser.center();
                addUser.show();
            }
        });

        // класс для удаления юзера
        class RetrieveUserhandler implements ClickHandler {

            public void onClick(ClickEvent event) {
                for (Long i : userDel) {
                    sendUserIdToServer(i);
                }
            }

            private synchronized void sendUserIdToServer(long id) {
                errorLabel.setText("");

                final String textToServer = userIdField.getText();

                textToServerLabel.setText(String.valueOf(textToServer));
                serverResponseLabel.setText("");


                userService.deleteUser(id, new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel
                                .addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : " + caught.toString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    public void onSuccess(Void aVoid) {
                        dialogBox.setText("Remote Procedure Call");
                        serverResponseLabel
                                .removeStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("Delete user" + textToServer);

                        dialogBox.center();
                        closeButton.setFocus(true);
                        userIdField.setText(null);
                        table.setRowData(findAll.alles());
                        table.setRowCount(findAll.count(), true);

                        table.setRowData(0, findAll.alles());
                        table.redraw();
                    }
                });
            }
        }
        ;
        RetrieveUserhandler retrieveUserhandler = new RetrieveUserhandler();
        retrieveButton.addClickHandler(retrieveUserhandler);

        // класс для редактирования юзера
        class EditUserPanel implements ClickHandler {

            public synchronized void onClick(ClickEvent arg0) {
                errorLabel.setText("");

                editUserBox.hide();
                String id = editUserId.getText();
                long userId = 0;
                userId = Long.parseLong(id);

                String userName = editUserNameField.getText();
                String userSurname = editUserSurnameField.getText();
                String userPhoneNumber = editUserPhoneNumber.getText();

                textToServerLabel.setText("User Id " + id);
                userService.updateUser(userId, userName, userSurname, userPhoneNumber, new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel
                                .addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : " + caught.toString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    public void onSuccess(Void noAnswer) {
                        dialogBox.setText("Server OK");
                        serverResponseLabel
                                .removeStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("OK");
                        dialogBox.center();
                        closeButton.setFocus(true);
                        table.setRowData(findAll.alles());
                        table.setRowCount(findAll.count(), true);

                        table.setRowData(0, findAll.alles());
                        table.redraw();
                    }
                });
            }
        }
        ;

        EditUserPanel editUserPanel = new EditUserPanel();
        update.addClickHandler(editUserPanel);

        // класс для добавления нового юзера
        class AddUserPanel implements ClickHandler {

            public synchronized void onClick(ClickEvent arg0) {
                errorLabel.setText("");

                addUser.hide();

                long userId = 0;
                for (int i = 0; i < findAll.count(); i++) {
                    if (userId <= (findAll.alles().get(i)).getUserId()) {
                        userId = (findAll.alles().get(i)).getUserId() + 1;
                    }
                }
                String userName = userNameField.getText();
                String userSurname = userSurnameField.getText();
                String userPhoneNumber = userPhoneNumberField.getText();

                textToServerLabel.setText("User Id " + String.valueOf(userId));
                userService.saveOrUpdateUser(userId, userName, userSurname, userPhoneNumber, new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel
                                .addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("An error occurred while attempting to contact the server. Please check your network connection and try again. The error is : " + caught.toString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    public void onSuccess(Void noAnswer) {
                        dialogBox.setText("Remote Procedure Call");
                        serverResponseLabel
                                .removeStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML("OK");
                        dialogBox.center();
                        closeButton.setFocus(true);
                        table.setRowData(findAll.alles());
                        table.setRowCount(findAll.count(), true);

                        table.setRowData(0, findAll.alles());
                        table.redraw();
                    }
                });
            }
        }
        ;

        AddUserPanel addUserPanel = new AddUserPanel();
        saveOrUpdateButton.addClickHandler(addUserPanel);
    }
}
